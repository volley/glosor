package waveram.midi;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class MidiTest {

    final int ticksPerQuarterNote = 96;
    final int wholeStep = ticksPerQuarterNote;
    final int halfStep = wholeStep / 2;

    final int baseNote = 36;
    final int dotNote = baseNote + 10;
    final int markNote = baseNote + 11;

    final int bpm = 146;

    boolean emitNumbers;
    boolean emitDot;
    boolean emitMark;
    boolean emitLyrics;

    int skippedTicks = 0;

    private void writeVariable(DataOutputStream dos, int value) throws IOException {
        if (value < 0) {
            throw new RuntimeException("Negative:" + value);
        }
        if (value > 0xFFFFFFF) {
            throw new RuntimeException("Too big:" + value);
        }

        int a = (value >> 21) & 0x7f;
        int b = (value >> 14) & 0x7f;
        int c = (value >> 7) & 0x7f;
        int d = value & 0x7f;

        if (a != 0) {
            dos.writeByte(a | 0x80);
            dos.writeByte(b | 0x80);
            dos.writeByte(c | 0x80);
        } else if (b != 0) {
            dos.writeByte(b | 0x80);
            dos.writeByte(c | 0x80);
        } else if (c != 0) {
            dos.writeByte(c | 0x80);
        }
        dos.writeByte(d);
    }

    private void writeNoteOn(int key, int velocity, DataOutputStream dataOutputStream) throws IOException {
        if (key < 0 || key > 127) {
            throw new RuntimeException("key:" + key);
        }
        if (velocity < 0 || velocity > 127) {
            throw new RuntimeException("velocity:" + velocity);
        }
        dataOutputStream.write(0x90); // note on, channel 0
        dataOutputStream.write(key);
        dataOutputStream.write(velocity);
    }

    private void writeNoteOff(int key, int velocity, DataOutputStream dataOutputStream) throws IOException {
        if (key < 0 || key > 127) {
            throw new RuntimeException("key:" + key);
        }
        if (velocity < 0 || velocity > 127) {
            throw new RuntimeException("velocity:" + velocity);
        }
        dataOutputStream.write(0x80); // note off, channel 0
        dataOutputStream.write(key);
        dataOutputStream.write(velocity);
    }

    private void writeSequence(int key, int velocity, DataOutputStream dos) throws IOException {
        writeVariable(dos, skippedTicks);
        writeNoteOn(key, velocity, dos);
        skippedTicks = 0;

        writeVariable(dos, halfStep);
        writeNoteOff(key, velocity, dos);
        skippedTicks = halfStep;
    }

    static String[] english = new String[]{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

    private static String toText(int v) {
        return english[v];
    }

    private int lyrPart = 0;

    private void emitLyrics(String part) {
        System.out.print(part);
        lyrPart++;
        if (lyrPart == 8) {
            System.out.println("");
            lyrPart = 0;
        } else {
            System.out.print(" ");
        }
    }

    private void writeTrack(DataOutputStream dataOutputStream) throws IOException {

        // Could use a fixed for-loop, but keeping it flexible in case we want variable-length chunks
        int tick = 0;
        while (true) {

            // Assuming five elements per time, meaning we rotate one per beat (5 vs 4)
            // "minute" "dot" "second" "second" "beep"
            // Beep will come 4 ticks into the future.
            final int beepTick = tick + 4;

            final int timeAtTick = (beepTick * 60) / bpm;
            final int minutesAtTick = timeAtTick / 60;
            final int secondsAtTick = timeAtTick % 60;
            final int secHi = secondsAtTick / 10;
            final int secLo = secondsAtTick % 10;

            if (minutesAtTick >= 10) {
                break;
            }

            if (emitNumbers) {
                writeSequence(minutesAtTick + baseNote, 100, dataOutputStream);
            } else {
                skippedTicks += wholeStep;
            }

            if (emitDot) {
                writeSequence(dotNote, 100, dataOutputStream);
            } else {
                skippedTicks += wholeStep;
            }

            if (emitNumbers) {
                writeSequence(secHi + baseNote, 110, dataOutputStream);
                writeSequence(secLo + baseNote, 120, dataOutputStream);
            } else {
                skippedTicks += wholeStep;
                skippedTicks += wholeStep;
            }

            if (emitMark) {
                writeSequence(markNote, 100, dataOutputStream);
            } else {
                skippedTicks += wholeStep;
            }

            if (emitLyrics) {
                emitLyrics(toText(minutesAtTick));
                emitLyrics("dot");
                emitLyrics(toText(secHi));
                emitLyrics(toText(secLo));
                emitLyrics("beep");
            }

            tick += 5;
        }

        writeVariable(dataOutputStream, halfStep);
    }

    private byte[] createTrack() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        writeTrack(dataOutputStream);
        dataOutputStream.flush();
        return byteArrayOutputStream.toByteArray();
    }

    private void main() throws IOException {
        /*
        emitNumbers = true;
        write("numbers.mid");
        emitNumbers = false;

        emitDot = true;
        write("dot.mid");
        emitDot = false;

        emitMark = true;
        write("mark.mid");
        emitMark = false;
        */
        emitLyrics = true;
        createTrack();
        emitLyrics = false;

    }

    public static void main(String[] args) throws IOException {
        new MidiTest().main();
    }

    void write(String name) throws IOException {
        skippedTicks = 0;
        try (final FileOutputStream fos = new FileOutputStream("G:\\Work\\audio\\Samples\\" + name)) {
            final DataOutputStream dataOutputStream = new DataOutputStream(fos);

            // Header
            dataOutputStream.writeBytes("MThd");
            dataOutputStream.writeInt(6);
            dataOutputStream.writeShort(0); // format = single multi-channel track
            dataOutputStream.writeShort(1); // number of tracks
            dataOutputStream.writeShort(ticksPerQuarterNote);

            // Track
            final byte[] track = createTrack();
            dataOutputStream.writeBytes("MTrk");
            dataOutputStream.writeInt(track.length);
            dataOutputStream.write(track);

            dataOutputStream.flush();
        }
    }

}
