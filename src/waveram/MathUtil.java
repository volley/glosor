package waveram;

public final class MathUtil {
    public static float clampUpper(float f) {
        return Math.min(f, 1f);
    }

    public static float distance(float dx, float dy) {
        return (float) Math.sqrt((dx * dx) + (dy * dy));
    }

    public static float distanceSquared(float dx, float dy) {
        return (dx * dx) + (dy * dy);
    }

    public static float distance(float x0, float y0, float x1, float y1) {
        return distance(x1 - x0, y1 - y0);
    }

    public static float distanceSquared(float x0, float y0, float x1, float y1) {
        return distanceSquared(x1 - x0, y1 - y0);
    }
}
