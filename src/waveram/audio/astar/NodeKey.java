package waveram.audio.astar;

import java.util.Objects;

class NodeKey {
    final Segment atSegment;
    final float atU; // must be the exact value in the junction, if any! we use operator==
    final int frameOffset; // how far ahead are we looking

    NodeKey(Segment atSegment, float atU, int frameOffset) {
        this.atSegment = atSegment;
        this.atU = atU;
        this.frameOffset = frameOffset;
    }

    @Override
    public String toString() {
        return "key{" + atSegment + " u" + Math.round(atU * 100) + "% off=" + frameOffset + "}";
    }

    NodeKey deriveFree(Segment nextSegment, float atU) {
        return new NodeKey(nextSegment, atU, frameOffset); // same frameOffset
    }

    NodeKey derive(Segment nextSegment, float atU) {
        return new NodeKey(nextSegment, atU, frameOffset + 1);
    }

    NodeKey derive(float atU) {
        return new NodeKey(atSegment, atU, frameOffset + 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NodeKey nodeKey = (NodeKey) o;
        return Float.compare(nodeKey.atU, atU) == 0 && frameOffset == nodeKey.frameOffset && atSegment.equals(nodeKey.atSegment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(atSegment, atU, frameOffset);
    }
}
