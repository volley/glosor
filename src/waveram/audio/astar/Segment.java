package waveram.audio.astar;

import waveram.MathUtil;

import java.util.ArrayList;
import java.util.List;

public class Segment {
    public float x0;
    public float y0;

    public float x1;
    public float y1;

    final List<Junction> exits = new ArrayList<>();

    final String label;

    public Segment(float x0, float y0, float x1, float y1, String label) {
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
        this.label = label;
    }

    public Junction addJunction(float atU, Segment toSegment, float toU, boolean isAirGap) {
        final Junction j = new Junction(this, toSegment, atU, toU, isAirGap);
        exits.add(j);
        return j;
    }

    public float getU(float px, float py) {
        // perf: could cache dx, dy and dx*dx+dy*dy
        float dx = x1 - x0;
        float dy = y1 - y0;
        if (dx == 0 && dy == 0) {
            throw new IllegalArgumentException();
        }
        return ((px - x0) * dx + (py - y0) * dy) / (dx * dx + dy * dy);
    }

    public float getClampedU(float px, float py) {
        return Math.max(Math.min(getU(px, py), 1f), 0f);
    }

    public float xForU(float u) {
        float dx = x1 - x0;
        return x0 + u * dx;
    }

    public float yForU(float u) {
        float dy = y1 - y0;
        return y0 + u * dy;
    }

    public float distanceToSquared(float px, float py, float u) {
        float x = xForU(u);
        float y = yForU(u);
        return MathUtil.distanceSquared(x, y, px, py);
    }

    @Override
    public String toString() {
        return label;
    }

    public Segment translate(float dx, float dy) {
        x0 += dx;
        y0 += dy;
        x1 += dx;
        y1 += dy;
        return this;
    }
}
