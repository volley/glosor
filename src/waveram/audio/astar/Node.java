package waveram.audio.astar;

class Node implements Comparable<Node> {
    private static int nextNodeTieBreaker = 0;

    public final NodeKey key;

    float g; // accumulated cost along path up until this point
    float gh; // best possible cost along remainder of path
    NodeKey cameFrom = null;

    final int tieBreaker = nextNodeTieBreaker++;

    public Node(NodeKey key) {
        this.key = key;
        g = Float.MAX_VALUE;
        gh = 0;
    }

    Node(NodeKey key, float g, float h) {
        this.key = key;
        this.g = g;
        this.gh = g + h;
    }

    @Override
    public String toString() {
        return key + "[g=" + g + ", gh=" + gh + "]";
    }

    @Override
    public int compareTo(Node o) {
        if (gh != o.gh) {
            return gh < o.gh ? -1 : 1;
        }
        if (tieBreaker != o.tieBreaker) {
            return tieBreaker < o.tieBreaker ? -1 : 1;
        }
        assert this == o;
        return 0;
    }
}
