package waveram.audio.astar;

public class Bounds {

    // Simple for now, could allow polygons and other shapes etc later
    private float x0 = Float.MAX_VALUE;
    private float y0 = Float.MAX_VALUE;
    private float x1 = Float.MIN_VALUE;
    private float y1 = Float.MIN_VALUE;

    public Bounds() {
    }

    public Bounds(float x0, float y0, float x1, float y1) {
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
    }

    public void extend(float x, float y) {
        x0 = Math.min(x, x0);
        y0 = Math.min(y, y0);
        x1 = Math.max(x, x1);
        y1 = Math.max(y, y1);
    }

    public void extend(Segment segment) {
        extend(segment.x0, segment.y0);
        extend(segment.x1, segment.y1);
    }

    public boolean isInBounds(float x, float y) {
        return x >= x0 && x <= x1 && y >= y0 && y <= y1;
    }

    private static final int OUT_LEFT = 1;
    private static final int OUT_TOP = 2;
    private static final int OUT_RIGHT = 4;
    private static final int OUT_BOTTOM = 8;

    // Adapted from Rectangle2D
    public boolean doesLineIntersect(float linex0, float liney0, float linex1, float liney1) {
        final int out2 = getOutMask(linex1, liney1);
        if (out2 == 0) {
            // Line end is inside
            return true;
        }

        while (true) {
            final int out1 = getOutMask(linex0, liney0);
            if (out1 == 0) {
                // Line start is inside
                return true;
            }
            if ((out1 & out2) != 0) {
                // Line start is on the same side(s) as line end
                return false;
            }

            // Advance the line start
            if ((out1 & (OUT_LEFT | OUT_RIGHT)) != 0) {
                final float x = (out1 & OUT_RIGHT) != 0 ? x1 : x0;
                liney0 = liney0 + (x - linex0) * (liney1 - liney0) / (linex1 - linex0);
                linex0 = x;
            } else {
                final float y = (out1 & OUT_BOTTOM) != 0 ? y1 : y0;
                linex0 = linex0 + (y - liney0) * (linex1 - linex0) / (liney1 - liney0);
                liney0 = y;
            }
        }
    }

    private int getOutMask(float px, float py) {
        int out = 0;
        if (px < x0) {
            out |= OUT_LEFT;
        } else if (px > x1) {
            out |= OUT_RIGHT;
        }
        if (py < y0) {
            out |= OUT_TOP;
        } else if (py > y1) {
            out |= OUT_BOTTOM;
        }
        return out;
    }

}
