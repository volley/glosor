package waveram.audio.astar;

import waveram.MathUtil;

import java.awt.geom.Line2D;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class AStar {

    final int frameCount;
    final int maxFrameLookAhead = 128;

    final float[] xs; // length == frameCount
    final float[] ys; // length == frameCount
    final float[] bestErrorAtFrame; // length == frameCount

    final List<Segment> segments;

    final Bounds bounds = new Bounds();

    public float xOut;
    public float yOut;

    public AStar(float[] xs, float[] ys, List<Segment> segments) {
        this.frameCount = xs.length;

        this.xs = xs;
        this.ys = ys;

        // Assuming static segments for now
        this.bestErrorAtFrame = new float[frameCount];
        for (int i = 0; i < frameCount; i++) {
            this.bestErrorAtFrame[i] = findLowestErrorSquared(segments, xs[i], ys[i], null);
        }

        this.segments = segments;

        for (Segment segment : this.segments) {
            bounds.extend(segment);
        }
    }

    private static class OpenSet {
        final Map<NodeKey, Node> allNodes = new HashMap<>(8 * 1024);
        final PriorityQueue<Node> openSet = new PriorityQueue<>(512);

        OpenSet(Node startNode) {
            allNodes.put(startNode.key, startNode);
            openSet.offer(startNode);
        }

        Node peekFirst() {
            return openSet.peek();
        }

        void removeFirst() {
            openSet.poll();
        }

        Node get(NodeKey cameFrom) {
            return allNodes.get(cameFrom);
        }

        void update(NodeKey nextHop, float tentativeG, float nextHeuristic, NodeKey cameFrom) {
            final Node node = allNodes.computeIfAbsent(nextHop, Node::new);
            if (node.g > tentativeG) {
                if (node.g != Float.MAX_VALUE) {
                    openSet.remove(node);
                }
                node.cameFrom = cameFrom;
                node.g = tentativeG;
                node.gh = tentativeG + nextHeuristic;
                openSet.offer(node);
            }
        }
    }

    public void solve(final int frameStart, Cursor cursor) {
        if (bounds.isInBounds(xs[frameStart], ys[frameStart])) {
            if (cursor.segment == null) {
                // This frame is in bounds, but we haven't entered the graph yet; see if we intersect with a segment
                Point point = new Point();
                Segment entrySegment = findClosestIntersectingSegment(segments, xs[frameStart], ys[frameStart], cursor.x, cursor.y, point);

                if (entrySegment == null) {
                    // No intersection so keep moving freely
                    cursor.x = xs[frameStart];
                    cursor.y = ys[frameStart];
                    xOut = cursor.x;
                    yOut = cursor.y;
                    return;
                }

                // Enter segment
                cursor.segment = entrySegment;
                cursor.x = point.x;
                cursor.y = point.y;
                cursor.u = cursor.segment.getClampedU(cursor.x, cursor.y);
                xOut = cursor.x;
                yOut = cursor.y;
            }
        } else if (cursor.segment == null) {
            // This frame is out of bounds, just like the one before
            Point point = null;
            Segment entrySegment = null;
            if (bounds.doesLineIntersect(cursor.x, cursor.y, xs[frameStart], ys[frameStart])) {
                // ...but there may be an intersection, so check.
                point = new Point();
                entrySegment = findClosestIntersectingSegment(segments, xs[frameStart], ys[frameStart], cursor.x, cursor.y, point);
            }
            if (entrySegment == null) {
                // There is no intersection so keep moving freely
                cursor.x = xs[frameStart];
                cursor.y = ys[frameStart];
                xOut = cursor.x;
                yOut = cursor.y;
                return;
            }

            // Enter segment
            cursor.segment = entrySegment;
            cursor.x = point.x;
            cursor.y = point.y;
            cursor.u = cursor.segment.getClampedU(cursor.x, cursor.y);
            xOut = cursor.x;
            yOut = cursor.y;
        } else {
            // This frame is out of bounds, let's see if we can leave without intersecting w/anything
            Point point = new Point();
            Segment exitSegment = findClosestIntersectingSegment(segments, cursor.x, cursor.y, xs[frameStart], ys[frameStart], point);
            if (exitSegment == null || exitSegment == cursor.segment) {
                cursor.segment = null;
                cursor.x = xs[frameStart];
                cursor.y = ys[frameStart];
                xOut = cursor.x;
                yOut = cursor.y;
                return;
            }
        }

        final int frameLookAhead = Math.min(frameCount - frameStart, maxFrameLookAhead);
        final int frameEnd = frameStart + frameLookAhead;

        final float[] heuristicAtFrameOffset = new float[frameLookAhead];
        for (int i = 0; i < frameLookAhead; i++) {
            heuristicAtFrameOffset[i] = calcHeuristic(bestErrorAtFrame, frameStart + i, frameEnd);
        }

        final NodeKey startNodeKey = new NodeKey(cursor.segment, cursor.u, 0);
        final Node startNode = new Node(startNodeKey, 0f, calcHeuristic(bestErrorAtFrame, frameStart, frameEnd));

        final OpenSet openSet = new OpenSet(startNode);
        while (true) {

            final Node current = openSet.peekFirst();
            if (current == null) {
                throw new IllegalStateException("No solution");
            }

            final NodeKey currentKey = current.key;
            if (currentKey.frameOffset == frameLookAhead - 1) {
                break;
            }

            openSet.removeFirst();

            final float nextPureX = xs[frameStart + currentKey.frameOffset + 1];
            final float nextPureY = ys[frameStart + currentKey.frameOffset + 1];
            final float nextHeuristic = heuristicAtFrameOffset[currentKey.frameOffset + 1];

            final Segment currentSegment = currentKey.atSegment;

            // Go straight to best point (or just stay if u:s are equal, that's fine too)
            {
                final float bestU = currentSegment.getClampedU(nextPureX, nextPureY);
                final NodeKey nextHop = currentKey.derive(bestU);
                final float tentativeG = current.g +
                        currentSegment.distanceToSquared(nextPureX, nextPureY, bestU);
                openSet.update(nextHop, tentativeG, nextHeuristic, currentKey);
            }

            // Loop over exits
            for (Junction exit : currentSegment.exits) {

                final float deltaU = Math.abs(currentKey.atU - exit.fromU);
                if (deltaU > 0.00001f) { // todo: what threshold should we apply?

                    // Next hop is the junction's exit point
                    final NodeKey nextHop = currentKey.derive(exit.fromU);
                    final float tentativeG = current.g +
                            currentSegment.distanceToSquared(nextPureX, nextPureY, exit.fromU);
                    openSet.update(nextHop, tentativeG, nextHeuristic, currentKey);

                } else if (exit.isAirGap) {

                    // Next hop is the entry point on the other side of an air gap
                    final NodeKey nextHop = currentKey.derive(exit.toSegment, exit.toU);
                    final float tentativeG = current.g +
                            exit.toSegment.distanceToSquared(nextPureX, nextPureY, exit.toU);
                    openSet.update(nextHop, tentativeG, nextHeuristic, currentKey);

                } else {

                    // Go straight over, same hop count
                    final NodeKey nextHop = currentKey.deriveFree(exit.toSegment, exit.toU);
                    final float tentativeG = current.g; // free hop
                    openSet.update(nextHop, tentativeG, nextHeuristic, currentKey);

                }
            }
        }

        // We don't care if it's the successor of the first, since there may be hops with no step in frameOffset
        Node node = openSet.peekFirst();
        for (; node.key.frameOffset > 1; node = openSet.get(node.cameFrom)) {
            if (node.cameFrom == null) {
                throw new IllegalStateException();
            }
        }

        cursor.segment = node.key.atSegment;
        cursor.u = node.key.atU;
        xOut = cursor.segment.xForU(cursor.u);
        yOut = cursor.segment.yForU(cursor.u);
    }

    /**
     * @param segments Segments to check for intersection
     * @param x0       Describes start of line to check against segments
     * @param y0       Describes start of line to check against segments
     * @param x1       Describes end of line to check against segments
     * @param y1       Describes end of line to check against segments
     * @param pointOut If a Segment is returned, this is updated with the intersection point
     * @return Intersecting Segment closest to x0/y0, or null
     */
    public static Segment findClosestIntersectingSegment(List<Segment> segments, float x1, float y1, float x0, float y0, Point pointOut) {
        final Point point = new Point();
        float lowestErrorSquared = Float.MAX_VALUE;
        Segment best = null;
        for (Segment segment : segments) {
            if (findIntersection(x0, y0, x1, y1, segment, point)) {
                float errorSquared = MathUtil.distanceSquared(x0, y0, point.x, point.y);
                if (lowestErrorSquared > errorSquared) {
                    lowestErrorSquared = errorSquared;
                    best = segment;
                    pointOut.x = point.x;
                    pointOut.y = point.y;
                }
            }
        }
        return best;
    }

    public static float findLowestErrorSquared(List<Segment> segments, float px, float py, Cursor cursor) {
        float lowestErrorSquared = Float.MAX_VALUE;
        for (Segment segment : segments) {
            float u = segment.getClampedU(px, py);
            float x = segment.xForU(u);
            float y = segment.yForU(u);
            float errorSquared = MathUtil.distanceSquared(x, y, px, py);
            if (lowestErrorSquared > errorSquared) {
                lowestErrorSquared = errorSquared;
                if (cursor != null) {
                    cursor.segment = segment;
                    cursor.u = u;
                    cursor.x = x;
                    cursor.y = y;
                }
            }
        }
        return lowestErrorSquared;
    }

    private static class Point {
        float x;
        float y;
    }

    private static boolean findIntersection(float x0, float y0, float x1, float y1, Segment s2, Point out) {
        if (!Line2D.linesIntersect(x0, y0, x1, y1, s2.x0, s2.y0, s2.x1, s2.y1)) {
            return false;
        }

        float a1 = y1 - y0;
        float b1 = x0 - x1;
        float c1 = a1 * x0 + b1 * y0;

        float a2 = s2.y1 - s2.y0;
        float b2 = s2.x0 - s2.x1;
        float c2 = a2 * s2.x0 + b2 * s2.y0;

        float delta = a1 * b2 - a2 * b1;

        out.x = (b2 * c1 - b1 * c2) / delta;
        out.y = (a1 * c2 - a2 * c1) / delta;

        return true;
    }

    /**
     * @param start First frame to count
     * @param end   End, first frame to NOT count
     */
    private static float calcHeuristic(float[] bestErrorAtFrame, int start, int end) {
        float sum = 0;
        for (; start < end; start++) {
            sum += bestErrorAtFrame[start];
        }
        return sum;
    }
}
