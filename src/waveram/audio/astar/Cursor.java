package waveram.audio.astar;

public class Cursor {

    public Segment segment;
    public float u;

    // Used if segment == null
    public float x;
    public float y;

    public Cursor() {
    }

    public Cursor(Segment startSegment, float startU) {
        segment = startSegment;
        u = startU;
    }

    @Override
    public String toString() {
        return segment + "@" + u;
    }

}
