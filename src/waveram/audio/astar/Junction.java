package waveram.audio.astar;

public class Junction {
    public final Segment fromSegment;
    public final Segment toSegment;
    public final float fromU;
    public final float toU;
    public final boolean isAirGap;

    public Junction(Segment from, Segment toSegment, float fromU, float toU, boolean isAirGap) {
        this.fromSegment = from;
        this.toSegment = toSegment;
        this.fromU = fromU;
        this.toU = toU;
        this.isAirGap = isAirGap;
    }

    @Override
    public String toString() {
        return fromSegment + "@" + fromU + "->" + toSegment + "@" + toU + (isAirGap ? "-gap" : "");
    }
}
