package waveram.audio;

public class PingPong implements Shape {

    private final Shape shape;

    public PingPong(Shape shape) {
        this.shape = shape;
    }

    @Override
    public void emit(float phase, Canvas out) {
        if (phase < 0.5f) {
            shape.emit(phase * 2f, out);
        } else {
            phase -= 0.5f;
            phase *= 2f;
            shape.emit(1.0f - phase, out);
        }
    }
}
