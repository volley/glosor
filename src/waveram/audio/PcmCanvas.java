package waveram.audio;

import java.io.*;

public class PcmCanvas implements Canvas, AutoCloseable {

    private static final boolean plot = false;

    private final DataOutputStream dos;

    private float amp = (32768 * 64);

    public PcmCanvas(DataOutputStream dos) {
        this.dos = dos;
    }

    public PcmCanvas(File file) throws FileNotFoundException {
        dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
    }

    public float getAmp() {
        return amp;
    }

    public void setAmp(float amp) {
        this.amp = amp;
    }

    /**
     * @param c0 -1..1, left
     * @param c1 -1..1, right
     */
    private void putLr(float c0, float c1) {
        try {
            dos.writeFloat(c0 * amp);
            dos.writeFloat(c1 * amp);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void put(float x, float y) {
        float c0 = y + x;
        float c1 = y - x;
        putLr(c0, c1);

        // For copy-paste into spreadsheet, for quick plotting
        if (plot) {
            System.out.println(String.valueOf(x).replace('.', ',')
                    + "; "
                    + String.valueOf(y).replace('.', ','));
        }
    }

    @Override
    public void close() throws Exception {
        dos.close();
    }
}
