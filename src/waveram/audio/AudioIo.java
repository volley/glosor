package waveram.audio;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class AudioIo {

    static FloatSamples loadSamples(File file) throws IOException, UnsupportedAudioFileException {
        try (final AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file)) {
            return loadSamples(audioInputStream);
        }
    }

    static FloatSamples loadSamples(AudioInputStream is) throws IOException {
        final AudioFormat format = is.getFormat();

        if (format.getSampleSizeInBits() == 16) {
            return loadSamples16(is, format.getChannels(), format.isBigEndian());
        }

        // We can support this, just need to code it
        throw new IOException("size:" + format.getSampleSizeInBits());
    }

    static void saveSamples(ShortSamples shortSamples, File file) throws IOException {
        final short[] pcm = shortSamples.samples;
        final byte[] buf = new byte[pcm.length * 2];
        for (int in = 0, out = 0; in < pcm.length; in++) {
            buf[out++] = (byte) (pcm[in] >> 8);
            buf[out++] = (byte) (pcm[in]);
        }

        final AudioFormat audioFormat = new AudioFormat(44100, 16, shortSamples.channels, true, true);
        final AudioInputStream audioInputStream = new AudioInputStream(new ByteArrayInputStream(buf), audioFormat, pcm.length / 2);
        AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, file);
    }

    private static FloatSamples loadSamples16(AudioInputStream is, int channels, boolean bigEndian) throws IOException {
        final List<short[]> shortBuffers = new ArrayList<>();

        int totalSampleCount = 0;

        {
            final byte[] readBuffer = new byte[128 * 1024 * channels];
            while (true) {

                int bytesInBuffer = 0;
                while (bytesInBuffer < readBuffer.length) {
                    int actual = is.read(readBuffer, bytesInBuffer, readBuffer.length - bytesInBuffer);
                    if (actual == -1) {
                        break;
                    }
                    bytesInBuffer += actual;
                }

                if (bytesInBuffer == 0) {
                    break;
                }

                if ((bytesInBuffer % (2 * channels)) != 0) {
                    throw new IOException("Uneven frame at end");
                }

                final short[] pcm = new short[bytesInBuffer / 2];
                int pcmPos = 0;
                if (bigEndian) {
                    for (int i = 0; i < bytesInBuffer; i += 2) {
                        pcm[pcmPos++] = (short) ((readBuffer[i] & 0xff) << 8 | ((readBuffer[i + 1]) & 0xff));
                    }
                } else {
                    for (int i = 0; i < bytesInBuffer; i += 2) {
                        pcm[pcmPos++] = (short) ((readBuffer[i] & 0xff) | ((readBuffer[i + 1]) & 0xff) << 8);
                    }
                }

                shortBuffers.add(pcm);
                totalSampleCount += pcm.length;
            }
        }

        final float[] floatBuffer = new float[totalSampleCount];
        int floatBufferIndex = 0;
        for (short[] shortBuffer : shortBuffers) {
            for (short s : shortBuffer) {
                floatBuffer[floatBufferIndex++] = s / 32768.0f; // could adjust dc...
            }
        }

        return new FloatSamples(floatBuffer, channels);
    }

}
