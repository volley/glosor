package waveram.audio;

public class ShortSamples {

    final short[] samples;
    final int channels;

    public ShortSamples(short[] samples, int channels) {
        this.samples = samples;
        this.channels = channels;
    }

    public FloatSamples toFloat() {
        final float[] floats = new float[samples.length];
        for (int i = 0; i < samples.length; i++) {
            floats[i] = (samples[i] + 0.5f) / 32767.5f;
        }
        return new FloatSamples(floats, channels);
    }

}
