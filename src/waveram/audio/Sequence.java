package waveram.audio;

import java.util.ArrayList;
import java.util.List;

class Sequence implements Shape {

    private static class Part {
        Shape shape;
        float duration;

        Part(Shape shape, float duration) {
            this.shape = shape;
            this.duration = duration;
        }
    }

    float totalDuration = 0f;
    List<Part> parts = new ArrayList<>();

    void add(Shape shape, float duration) {
        totalDuration += duration;
        parts.add(new Part(shape, duration));
    }

    @Override
    public void emit(final float phaseIn, Canvas out) {
        float phase = phaseIn * totalDuration;
        for (Part part : parts) {
            if (phase <= part.duration) {
                part.shape.emit(phase / part.duration, out);
                return;
            }
            phase -= part.duration;
        }
        throw new RuntimeException("Rounding error?");
    }

}
