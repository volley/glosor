package waveram.audio;

interface Canvas {
    void put(float x, float y);
}
