package waveram.audio;

class Line implements Shape {

    float x0, y0;
    float x1, y1;

    public Line() {
    }

    public Line(float x0, float y0, float x1, float y1) {
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
    }

    /**
     * @param phase 0..1
     */
    public void emit(float phase, Canvas out) {
        final float dx = x1 - x0;
        final float dy = y1 - y0;
        out.put(x0 + phase * dx, y0 + phase * dy);
    }
}
