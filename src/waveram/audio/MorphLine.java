package waveram.audio;

class MorphLine implements Shape {
    float x0a;
    float x1a;

    float x0b;
    float x1b;

    float y0a;
    float y1a;

    float y0b;
    float y1b;

    float morph; // 0 (a) .. 1 (b)

    public MorphLine(float x0a, float y0a, float x1a, float y1a,
                     float x0b, float y0b, float x1b, float y1b) {
        this.x0a = x0a;
        this.x1a = x1a;
        this.x0b = x0b;
        this.x1b = x1b;
        this.y0a = y0a;
        this.y1a = y1a;
        this.y0b = y0b;
        this.y1b = y1b;
    }

    @Override
    public void emit(float phase, Canvas out) {
        float x0 = (x0a + (x0b - x0a) * morph);
        float x1 = (x1a + (x1b - x1a) * morph);
        float y0 = (y0a + (y0b - y0a) * morph);
        float y1 = (y1a + (y1b - y1a) * morph);
        final float dx = x1 - x0;
        final float dy = y1 - y0;
        out.put(x0 + phase * dx, y0 + phase * dy);
    }

}
