package waveram.audio;

class FloatSamples {

    final float[] samples;
    final int channels;

    FloatSamples(float[] samples, int channels) {
        this.samples = samples;
        this.channels = channels;

        if (getFrames() * channels != this.samples.length) {
            throw new IllegalArgumentException();
        }
    }

    void lrToSpace() {
        if (channels != 2) {
            throw new UnsupportedOperationException();
        }

        for (int i = 0; i < samples.length; i += 2) {
            float ch0 = samples[i];
            float ch1 = samples[i + 1];
            samples[i] = (ch1 - ch0) / 2f;
            samples[i + 1] = (ch0 + ch1) / 2f;
        }
    }

    void spaceToLr() {
        if (channels != 2) {
            throw new UnsupportedOperationException();
        }

        for (int i = 0; i < samples.length; i += 2) {
            float x = samples[i];
            float y = samples[i + 1];
            samples[i] = (y - x);
            samples[i + 1] = (y + x);
        }
    }

    int getFrames() {
        return this.samples.length / channels;
    }

    FloatSamples getChannel(int channel) {
        if (channel >= channels || channel < 0) {
            throw new IllegalArgumentException("wanted=" + channel + ", available=" + channels);
        }
        float[] fs = new float[samples.length / channels];
        for (int i = channel, j = 0; j < fs.length; i += channels, j++) {
            fs[j] = samples[i];
        }
        return new FloatSamples(fs, 1);
    }

    ShortSamples normalizeTo16() {
        float min = 0;
        float max = 0;

        for (float sample : samples) {
            min = Math.min(min, sample);
            max = Math.max(max, sample);
        }

        float factorMin = (min != 0f) ? -32768.0f / min : Float.MAX_VALUE;
        float factorMax = (max != 0f) ? 32767.0f / max : Float.MAX_VALUE;
        float factor = Math.min(factorMin, factorMax);

        final short[] shorts = new short[samples.length];
        for (int i = 0; i < samples.length; i++) {
            shorts[i] = (short) (samples[i] * factor); // todo: +0.5f to round? write tests.. maybe even dither?
        }

        return new ShortSamples(shorts, channels);
    }

    static FloatSamples join(FloatSamples c0, FloatSamples c1) {
        if (c0.getFrames() != c1.getFrames()) {
            throw new IllegalArgumentException();
        }
        if (c0.channels != 1) {
            // Can support this, just haven't coded it
            throw new UnsupportedOperationException();
        }
        if (c1.channels != 1) {
            // Can support this, just haven't coded it
            throw new UnsupportedOperationException();
        }

        final int frames = c0.getFrames();

        final float[] joined = new float[frames];
        for (int readIndex = 0, writeIndex = 0; writeIndex < joined.length; readIndex++) {
            joined[writeIndex++] = c0.samples[readIndex];
            joined[writeIndex++] = c1.samples[readIndex];
        }

        return new FloatSamples(joined, 2);
    }

}
