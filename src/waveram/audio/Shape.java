package waveram.audio;

interface Shape {
    void emit(float phase, Canvas out);
}
