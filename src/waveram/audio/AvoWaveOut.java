package waveram.audio;

import waveram.audio.astar.AStar;
import waveram.audio.astar.Cursor;
import waveram.audio.astar.Segment;
import waveram.video.Engine;
import waveram.video.Song;
import waveram.video.VideoFormat;
import waveram.video.algos.AvoWaveAlgorithm;
import waveram.video.util.TimeSampler;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class AvoWaveOut {

    @SuppressWarnings("RedundantIfStatement")
    private static boolean processSecond(int second) {
        if (second >= 109 && second <= 117) {
            return true;
        }
        if (second >= 312 && second <= 320) {
            return true;
        }
        return false;
    }

    private void process(File inFile, File outFile, List<Segment> segments)
            throws UnsupportedAudioFileException, IOException {

        final String tracePrefix = "AvoWaveOut.process [" + outFile.getName() + "]: ";

        FloatSamples floats = AudioIo.loadSamples(inFile);
        floats.lrToSpace();

        // todo: split and join using FloatSamples code
        final int frameCount = floats.getFrames();
        final float[] xs = new float[frameCount];
        final float[] ys = new float[frameCount];
        for (int i = 0, j = 0; i < frameCount; i++) {
            xs[i] = floats.samples[j++];
            ys[i] = floats.samples[j++];
        }

        final Cursor cursor = new Cursor();
        AStar.findLowestErrorSquared(segments, 0f, 0f, cursor);

        final AStar aStar = new AStar(xs, ys, segments);

        final TimeSampler timeSampler = new TimeSampler();

        for (int frameStart = 0; frameStart < frameCount; frameStart++) {
            if ((frameStart % 8192) == 0) {
                log(tracePrefix + ((frameStart * 100) / frameCount) + "%\r");
            }
            int second = frameStart / 44100;
            if (processSecond(second)) {
                aStar.solve(frameStart, cursor);
                xs[frameStart] = aStar.xOut;
                ys[frameStart] = aStar.yOut;
            } else {
                xs[frameStart] = 0;
                ys[frameStart] = 0;
            }
        }
        //System.out.println("AvoWaveOut.process: 100%");

        log(tracePrefix + frameCount + " sample frames processed in " + timeSampler.pollMicros() + " micros");

        for (int i = 0, j = 0; i < frameCount; i++) {
            floats.samples[j++] = -xs[i];
            floats.samples[j++] = ys[i];
        }

        floats.spaceToLr();
        AudioIo.saveSamples(floats.normalizeTo16(), outFile);
        log(tracePrefix + "Saved as " + outFile);
    }

    private void log(String text) {
        synchronized (System.out) {
            System.out.println(text);
        }
    }

    private static boolean findIntersection(float x0, float y0, float x1, float y1,
                                            float x2, float y2, float x3, float y3,
                                            float[] result) {
        final float bx = x1 - x0;
        final float by = y1 - y0;
        final float dx = x3 - x2;
        final float dy = y3 - y2;

        float bDotDPerpend = bx * dy - by * dx;
        if (bDotDPerpend == 0) {
            return false;
        }

        final float cx = x2 - x0;
        final float cy = y2 - y0;
        final float u = (cx * dy - cy * dx) / bDotDPerpend;

        result[0] = x0 + u * bx;
        result[1] = y0 + u * by;
        return true;
    }

    private static class Segments {
        final List<Segment> list = new ArrayList<>();

        Segment addSegment(float x1, float y1, float x2, float y2, String label) {
            final Segment segment = new Segment(x1, y1, x2, y2, label);
            list.add(segment);
            return segment;
        }

        Segment addSegment(float x2, float y2, String label) {
            final Segment last = list.get(list.size() - 1);
            final Segment segment = new Segment(last.x1, last.y1, x2, y2, label);
            list.add(segment);
            return segment;
        }

        void addJunctionsAtIntersections() {
            final float[] xyOut = new float[2];

            final float thres = 0.000001f;
            final float loU = -thres;
            final float hiU = 1.0f + thres;

            for (int i = 0; i < list.size(); i++) {
                for (int j = i + 1; j < list.size(); j++) {
                    final Segment segmentA = list.get(i);
                    final Segment segmentB = list.get(j);

                    if (findIntersection(segmentA.x0, segmentA.y0, segmentA.x1, segmentA.y1,
                            segmentB.x0, segmentB.y0, segmentB.x1, segmentB.y1, xyOut)) {

                        float uA = segmentA.getU(xyOut[0], xyOut[1]);
                        float uB = segmentB.getU(xyOut[0], xyOut[1]);

                        if (uA >= loU && uA <= hiU && uB >= loU && uB <= hiU) {
                            segmentA.addJunction(uA, segmentB, uB, false);
                            segmentB.addJunction(uB, segmentA, uA, false);
                        }
                    }
                }
            }
        }
    }

    static class Variant {
        final String label;
        final Segments segments = new Segments();

        Variant(String label) {
            this.label = label;
        }

        void paste(Segments in, float dx, float dy) {
            for (Segment segment : in.list) {
                this.segments.list.add(segment.translate(dx, dy));
            }
        }
    }

    private static File getOutFile(Variant variant, File inFile) {
        String name = inFile.getName();
        if (name.endsWith(".wav")) {
            name = name.substring(0, name.length() - 4);
        }
        name += "_" + variant.label + ".wav";
        return new File(inFile.getParentFile(), name);
    }

    private void foo() throws IOException, UnsupportedAudioFileException, InterruptedException {
        //final File inFile = new File("G:\\Work\\audio\\Ableton\\Early_board Project\\grainer\\pad1_in.wav");
        final File inFile = new File("G:\\Work\\audio\\Ableton\\Early_board Project\\Early_board_78_instrumental_2022-06-08.wav");

        List<Variant> variants = new ArrayList<>();
        variants.add(createVariant(0.1f));
        //variants.add(createVariant(0.075f));
        variants.add(createVariant(0.05f));
        //variants.add(createVariant(0.03f));

        for (Variant variant : variants) {
            final File outFile = getOutFile(variant, inFile);
            //noinspection EmptyTryBlock,unused
            try (final FileOutputStream fileOutputStream = new FileOutputStream(outFile, true)) {
                // Just checking that file is writeable
            }
        }

        {
            final int numThreads = Math.min(Runtime.getRuntime().availableProcessors(), variants.size());
            final ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

            for (Variant variant : variants) {
                final File outFile = getOutFile(variant, inFile);
                executorService.execute(() -> {
                    System.out.println("Variant: " + variant.label);
                    try {
                        process(inFile, outFile, variant.segments.list);
                    } catch (Throwable t) {
                        t.printStackTrace();
                        System.exit(1);
                    }
                });
            }

            executorService.shutdown();
            executorService.awaitTermination(1, TimeUnit.DAYS);
        }

        System.out.println("Rendering...");
        for (Variant variant : variants) {
            final Engine engine = new Engine();

            // 146 bpm
            final Song grains = new Song("Grains_" + variant.label, getOutFile(variant, inFile));

            //engine.render(bond, new AvoWaveAlgorithm(130), Path.of("f:\\temp\\render"), VideoFormat.fullHd24);

            //engine.render(avoWave, new AvoWaveAlgorithm(146), Path.of("f:\\temp\\render"), VideoFormat.fullHd24.flip());
            //engine.render(avoWave, new AvoWaveAlgorithm(146), Path.of("f:\\temp\\render"), VideoFormat.fullHd24);

            engine.render(grains, new AvoWaveAlgorithm(146), FileSystems.getDefault().getPath("f:\\temp\\render"), VideoFormat.fullHd24);
        }
    }

    private Variant createVariant(float dim) {
        float w = dim * 0.4f;
        float c = 0.0f;
        float hi = -dim;
        //noinspection UnnecessaryLocalVariable
        float lo = dim;

        float dx = w * 1.05f;

        final Variant main = new Variant("av" + dim);
        {
            // === A ===
            float aU = 0.4f;
            final Segment aLeftSegment = main.segments.addSegment(-w, lo, c, hi, "a_left").translate(-dx, 0);
            final Segment aRightSegment = main.segments.addSegment(w, lo, c, hi, "a_right").translate(-dx, 0);
            final Segment aMidSegment = main.segments.addSegment(aLeftSegment.xForU(aU), aLeftSegment.yForU(aU),
                    aRightSegment.xForU(aU), aRightSegment.yForU(aU), "a_mid");

            // === V ===
            final Segment vLeftSegment = main.segments.addSegment(c, lo, -w, hi, "v_left").translate(0.0f, 0);
            final Segment vRightSegment = main.segments.addSegment(c, lo, w, hi, "v_right").translate(0.0f, 0);

            // === O ===
            float oHi = hi;
            float oLo = lo;
            float oW = w;
            float oIW = oW * 0.59f;
            float oIHi = oHi * 0.59f;
            float oILo = oLo * 0.59f;

            main.segments.addSegment(-oIW, oHi, oIW, oHi, "o_top").translate(dx, 0);
            main.segments.addSegment(-oIW, oLo, oIW, oLo, "o_bot").translate(dx, 0);
            main.segments.addSegment(-oW, oILo, -oW, oIHi, "o_left").translate(dx, 0);
            main.segments.addSegment(oW, oILo, oW, oIHi, "o_right").translate(dx, 0);
            main.segments.addSegment(-oW, oIHi, -oIW, oHi, "o_TL").translate(dx, 0);
            main.segments.addSegment(-oW, oILo, -oIW, oLo, "o_BL").translate(dx, 0);
            main.segments.addSegment(oW, oIHi, oIW, oHi, "o_TR").translate(dx, 0);
            main.segments.addSegment(oW, oILo, oIW, oLo, "o_BR").translate(dx, 0);

            {
                float hiU = 0.3f;
                float wavex = aLeftSegment.xForU(hiU);// - 0.00001f;
                float wavexEnd = aRightSegment.xForU(hiU);// + 0.00001f;
                float hiy = aLeftSegment.yForU(hiU);// - 0.00001f;
                float loy = aLeftSegment.yForU(0.0f);

                float kernx = w * 0.1f / 3f;
                float stepx = (wavexEnd - wavex - 2 * kernx) / 6;

                // W
                main.segments.addSegment(wavex, hiy, wavex + stepx, loy, "W0");
                wavex += stepx;
                main.segments.addSegment(wavex + stepx / 2f, (loy + hiy) / 2f, "W1");
                wavex += stepx / 2f;
                main.segments.addSegment(wavex + stepx / 2f, loy, "W2");
                wavex += stepx / 2f;
                final Segment wSegment = main.segments.addSegment(wavex + stepx, hiy, "W3");
                wavex += kernx;

                // A
                final Segment aSegment0 = main.segments.addSegment(wavex, loy, wavex + stepx, hiy, "A0");
                wavex += stepx;
                final Segment aSegment1 = main.segments.addSegment(wavex + stepx, loy, wavex, hiy, "A1");
                main.segments.addSegment(aSegment0.xForU(aU), aSegment0.yForU(aU),
                        aSegment1.xForU(aU), aSegment1.yForU(aU), "AM");
                wavex += kernx;

                // V
                final Segment vSegment0 = main.segments.addSegment(wavex, hiy, wavex + stepx, loy, "V0");
                wavex += stepx;
                final Segment vSegment1 = main.segments.addSegment(wavex + stepx, hiy, "V1");
                wavex += stepx;

                // E
                final Segment eSegment0 = main.segments.addSegment(wavex, loy, "E0");
                main.segments.addSegment(eSegment0.xForU(0), eSegment0.yForU(0),
                        eSegment0.xForU(0) + stepx, eSegment0.yForU(0), "E1");
                main.segments.addSegment(eSegment0.xForU(aU), eSegment0.yForU(aU),
                        eSegment0.xForU(aU) + (stepx * 0.8f), eSegment0.yForU(aU), "E2");
                main.segments.addSegment(eSegment0.xForU(1), eSegment0.yForU(1),
                        eSegment0.xForU(1) + stepx, eSegment0.yForU(1), "E3");

                wSegment.addJunction(0, aSegment0, 0, true);
                wSegment.addJunction(1, aSegment0, 1, true);
                aSegment0.addJunction(0, wSegment, 0, true);
                aSegment0.addJunction(1, wSegment, 1, true);

                aSegment1.addJunction(0, vSegment0, 0, true);
                aSegment1.addJunction(1, vSegment0, 1, true);
                vSegment0.addJunction(0, aSegment1, 0, true);
                vSegment0.addJunction(1, aSegment1, 1, true);
            }

            main.segments.addJunctionsAtIntersections();

            // === Connect A with V ===
            aRightSegment.addJunction(0, vLeftSegment, 0, true);
            aRightSegment.addJunction(aU, vLeftSegment, aU, true);
            aRightSegment.addJunction(1, vLeftSegment, 1, true);
            vLeftSegment.addJunction(0, aRightSegment, 0, true);
            vLeftSegment.addJunction(1, aRightSegment, 1, true);
        }
        return main;
    }

    public static void main(String[] args) throws IOException, UnsupportedAudioFileException, InterruptedException {
        //final PcmCanvas canvas = new PcmCanvas(new File("out.pcm"));
        final AvoWaveOut avoWaveOut = new AvoWaveOut();
        avoWaveOut.foo();
    }

}
