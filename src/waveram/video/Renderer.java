package waveram.video;

public interface Renderer {

    /**
     * Called once for every Graphics2D (any thread)
     */
    void initBuffer(ImageBuffer imageBuffer);

    /**
     * Called before call to render (any thread)
     */
    void resetBuffer(ImageBuffer imageBuffer);

    /**
     * Called to render (any thread)
     */
    void render(ImageBuffer imageBuffer, int imageIndex);

}
