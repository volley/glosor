package waveram.video;

import waveram.video.algos.AvoWaveAlgorithm;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;

public class Main {
    public static void main(String[] args) throws IOException, UnsupportedAudioFileException, InterruptedException {
        final Engine engine = new Engine();

        // 146 bpm
        //final Song avoWave = new Song("AvoWave",
        //        new File("G:\\Work\\audio\\Ableton\\Early_board Project\\Early_board_40_2022-04-18.wav"));
        final Song avoWave = new Song("AvoWave",
                new File("G:\\Work\\audio\\Ableton\\Early_board Project\\Avowave (E78 instrumental).wav"));

        // 130 bpm
        final Song bond = new Song("Bond",
                new File("G:\\Work\\audio\\Ableton\\old\\_done\\James Bond (Reftel)\\live_2005_05_16\\volley-James_Bond.wav"));

        final Song grains = new Song("Grains",
                new File("G:\\Work\\audio\\Ableton\\Early_board Project\\pad1_out_av.wav"));

        //engine.render(bond, new AvoWaveAlgorithm(130), Path.of("f:\\temp\\render"), VideoFormat.fullHd24);

        //engine.render(avoWave, new AvoWaveAlgorithm(146), Path.of("f:\\temp\\render"), VideoFormat.fullHd24);
        engine.render(avoWave, new AvoWaveAlgorithm(146), FileSystems.getDefault().getPath("f:\\temp\\render"), VideoFormat.fullHd24.flip());
        engine.render(avoWave, new AvoWaveAlgorithm(146), FileSystems.getDefault().getPath("f:\\temp\\render"), new VideoFormat(24, 3840, 2160));
        //engine.render(avoWave, new AvoWaveAlgorithm(146), Path.of("f:\\temp\\render"), new VideoFormat(24, 3840, 2160).flip());

        //engine.render(grains, new AvoWaveAlgorithm(146), Path.of("f:\\temp\\render"), VideoFormat.fullHd24);

        //engine.render(avoWave, new StarAlgorithm(), Path.of("f:\\temp\\render"), VideoFormat.fullHd24);
        //engine.render(avoWave, new JourneyAlgorithm(146), Path.of("f:\\temp\\render"), VideoFormat.fullHd24);
    }

}
