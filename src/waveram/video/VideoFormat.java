package waveram.video;

public class VideoFormat {
    public final int fps;
    public final int w;
    public final int h;

    public static VideoFormat fullHd24 = new VideoFormat(24, 1920, 1080);

    public VideoFormat(int fps, int w, int h) {
        this.fps = fps;
        this.w = w;
        this.h = h;
    }

    public VideoFormat flip() {
        return new VideoFormat(fps, h, w);
    }

    public String getFolderName() {
        if (fps == 24 && w == 1920 && h == 1080) {
            return "fullHd";
        }
        return w + "," + h + "@" + fps;
    }

    @Override
    public String toString() {
        return getFolderName();
    }
}
