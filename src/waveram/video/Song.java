package waveram.video;

import java.io.File;

/**
 * Specifies a song to be used as input.
 */
public class Song {
    private final String name;
    private final File inputFile;

    public Song(String name, File inputFile) {
        this.name = name;
        this.inputFile = inputFile;
    }

    public String getShortName() {
        return name;
    }

    public File getInputFile() {
        return inputFile;
    }
}
