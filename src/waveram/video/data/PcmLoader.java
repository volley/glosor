package waveram.video.data;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static javax.sound.sampled.AudioSystem.getAudioInputStream;

public final class PcmLoader {

    public Pcm loadPcm(File audioFile, int videoFps, boolean carryOver) throws UnsupportedAudioFileException, IOException {
        try (final AudioInputStream audioInputStream = getAudioInputStream(audioFile)) {
            checkFormat(audioInputStream.getFormat());

            final int audioFps = (int) (audioInputStream.getFormat().getSampleRate());
            if (audioFps != 44100) {
                throw new UnsupportedAudioFileException("sampleRate:" + audioFps);
            }

            final List<PcmForImage> pcmList = loadPcm(videoFps, audioFps, carryOver, audioInputStream);

            return new Pcm(videoFps, audioFps, pcmList);
        }
    }

    private static List<PcmForImage> loadPcm(int videoFps, int audioFps, boolean carryOver, InputStream inputStream) throws IOException {
        final int maxAudioFramesPerImage = ((audioFps + videoFps - 1) / videoFps) + (carryOver ? 1 : 0);
        final byte[] frameBuf = new byte[maxAudioFramesPerImage * 2 * 2];

        final List<PcmForImage> pcmList = new ArrayList<>();

        boolean isEof = false;
        for (int imageIndex = 0, totalAudioFramesRead = 0; !isEof; imageIndex++) {

            final int targetAudioFramesRead = ((imageIndex + 1) * audioFps) / videoFps;
            final int audioFramesInImage = targetAudioFramesRead - totalAudioFramesRead;
            totalAudioFramesRead += audioFramesInImage;

            final int audioBytesInImage = audioFramesInImage * 4;
            for (int bytesRead = 0; bytesRead < audioBytesInImage; ) {
                int actual = inputStream.read(frameBuf, bytesRead, audioBytesInImage - bytesRead);
                if (actual == -1) {
                    isEof = true;
                    Arrays.fill(frameBuf, bytesRead, audioBytesInImage, (byte) 0);
                    break;
                }
                bytesRead += actual;
            }

            final boolean hasCarryOver = carryOver && !pcmList.isEmpty();
            final int carryOverLength = hasCarryOver ? 1 : 0;

            // todo: switch to int, so we can support 24-bit and 32-bit?
            final short[] pcmL = new short[audioFramesInImage + carryOverLength];
            final short[] pcmR = new short[audioFramesInImage + carryOverLength];

            // Copy carry-over
            if (hasCarryOver) {
                final PcmForImage lastPcm = pcmList.get(pcmList.size() - 1);
                pcmL[0] = lastPcm.getLastL();
                pcmR[0] = lastPcm.getLastR();
            }

            // Copy new PCM
            for (int i = 0, offset = carryOverLength; i < audioBytesInImage; i += 4, offset++) {
                pcmL[offset] = (short) ((frameBuf[i] & 0xff) | ((frameBuf[i + 1]) & 0xff) << 8);
                pcmR[offset] = (short) ((frameBuf[i + 2] & 0xff) | ((frameBuf[i + 3]) & 0xff) << 8);
            }

            pcmList.add(new PcmForImage(imageIndex, pcmL, pcmR));
        }

        return pcmList;
    }

    private static void checkFormat(AudioFormat format) throws IOException {
        if (format.getChannels() != 2 ||
                format.getFrameRate() != 44100 ||
                format.getSampleRate() != 44100 ||
                format.getSampleSizeInBits() != 16 ||
                format.isBigEndian() ||
                format.getEncoding() != AudioFormat.Encoding.PCM_SIGNED) {
            throw new IOException(format.toString());
        }
    }

}
