package waveram.video.data;

import java.util.List;

public class Compressor {

    public interface ZoomFactorGetter {
        float getZoomFactor(int index);
    }

    public static void compress(List<SpaceForImage> list, final float maxXyValue,
                                final float maxZoomInFactor, final float maxZoomOutFactor) {
        compress(list, maxXyValue, (index) -> maxZoomInFactor, (index) -> maxZoomOutFactor);
    }

    public static void compress(List<SpaceForImage> list, final float maxXyValue,
                                final ZoomFactorGetter maxZoomInFactor, final ZoomFactorGetter maxZoomOutFactor) {

        final float[] zoomFactor = new float[list.size()];

        // Find ideal zoom factors
        for (SpaceForImage spaceForImage : list) {
            final float maxAbsInSpace = spaceForImage.getMaxAbs();
            if (maxAbsInSpace == 0.0f) {
                throw new RuntimeException();
            }
            zoomFactor[spaceForImage.getImageIndex()] = maxXyValue / maxAbsInSpace;
        }

        { // Forward pass to limit zoom-in rate
            float at = zoomFactor[0];
            for (int i = 1; i < zoomFactor.length; i++) {
                if (zoomFactor[i] <= at) {
                    at = zoomFactor[i]; // instant
                } else {
                    final float limit = at * maxZoomInFactor.getZoomFactor(i);
                    if (zoomFactor[i] > limit) {
                        zoomFactor[i] = limit;
                    }
                    at = zoomFactor[i];
                }
            }
        }

        { // Reverse pass to limit zoom-out rate
            float at = zoomFactor[zoomFactor.length - 1];
            for (int i = zoomFactor.length; i-- > 0; ) {
                if (zoomFactor[i] <= at) {
                    at = zoomFactor[i]; // instant
                } else {
                    final float limit = at * maxZoomOutFactor.getZoomFactor(i);
                    if (zoomFactor[i] > limit) {
                        zoomFactor[i] = limit;
                    }
                    at = zoomFactor[i];
                }
            }
        }

        // Apply
        for (SpaceForImage spaceForImage : list) {
            final float factor = zoomFactor[spaceForImage.getImageIndex()];
            spaceForImage.scale(factor);
        }
    }

}
