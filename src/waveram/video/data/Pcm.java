package waveram.video.data;

import java.util.ArrayList;
import java.util.List;

public class Pcm {

    private final int videoFps;
    private final int audioFps;

    private final List<PcmForImage> pcmList;

    public Pcm(int videoFps, int audioFps, List<PcmForImage> pcmList) {
        this.videoFps = videoFps;
        this.audioFps = audioFps;
        this.pcmList = new ArrayList<>(pcmList);
    }

    public int getVideoFps() {
        return videoFps;
    }

    public int getAudioFps() {
        return audioFps;
    }

    public List<PcmForImage> getPcmList() {
        return pcmList;
    }

}
