package waveram.video.data;

public enum ScopeMode {
    Vertical,
    Horizontal
}
