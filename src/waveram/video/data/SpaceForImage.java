package waveram.video.data;

import java.util.Arrays;

public class SpaceForImage {

    private final int imageIndex;
    private final float[] xs;
    private final float[] ys;

    private float zoom = 1.0f;

    public SpaceForImage(int imageIndex, float[] xs, float[] ys) {
        this.imageIndex = imageIndex;
        this.xs = xs;
        this.ys = ys;
    }

    public SpaceForImage copy() {
        final SpaceForImage spaceForImage = new SpaceForImage(imageIndex,
                Arrays.copyOf(xs, xs.length), Arrays.copyOf(ys, ys.length));
        spaceForImage.zoom = this.zoom;
        return spaceForImage;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public int getFrameCount() {
        return xs.length;
    }

    public float[] getXs() {
        return xs;
    }

    public float[] getYs() {
        return ys;
    }

    public float getMaxAbsX() {
        float max = 0;
        for (float x : xs) {
            max = Float.max(max, Math.abs(x));
        }
        return max;
    }

    public float getMaxAbsY() {
        float max = 0;
        for (float y : ys) {
            max = Float.max(max, Math.abs(y));
        }
        return max;
    }

    public float getMaxAbs() {
        return Float.max(getMaxAbsX(), getMaxAbsY());
    }

    public void scale(float factor) {
        for (int i = 0; i < xs.length; i++) {
            xs[i] *= factor;
        }
        for (int i = 0; i < ys.length; i++) {
            ys[i] *= factor;
        }
        zoom *= factor;
    }

    public void scaleX(float factor) {
        for (int i = 0; i < xs.length; i++) {
            xs[i] *= factor;
        }
    }

    public void translate(float tx, float ty) {
        for (int i = 0; i < xs.length; i++) {
            xs[i] += tx;
        }
        for (int i = 0; i < ys.length; i++) {
            ys[i] += ty;
        }
    }

    public float getZoom() {
        return zoom;
    }

    public static SpaceForImage create(PcmForImage pcmForImage, ScopeMode scopeMode) {
        final int frameCount = pcmForImage.getFrameCount();
        final float[] xs = new float[frameCount];
        final float[] ys = new float[frameCount];
        final short[] ch0 = pcmForImage.getPcmL();
        final short[] ch1 = pcmForImage.getPcmR();

        for (int i = 0; i < frameCount; i++) {
            switch (scopeMode) {
                case Vertical:
                    ys[i] = ((int) ch0[i] + (int) ch1[i]) + 1; // Range -65535 .. 65535
                    xs[i] = (int) ch0[i] - (int) ch1[i]; // Range -65535 .. 65535
                    break;
                case Horizontal:
                    ys[i] = ((int) ch0[i] - (int) ch1[i]) + 1; // Range -65535 .. 65535
                    xs[i] = (int) ch0[i] + (int) ch1[i]; // Range -65535 .. 65535
                    break;
            }
        }
        return new SpaceForImage(pcmForImage.getImageIndex(), xs, ys);
    }
}
