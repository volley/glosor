package waveram.video.data;

public class PcmForImage {

    private final int imageIndex;
    private final short[] pcmL;
    private final short[] pcmR;

    public PcmForImage(int imageIndex, short[] pcmL, short[] pcmR) {
        if (imageIndex < 0) {
            throw new IllegalArgumentException("imageIndex:" + imageIndex);
        }
        if (pcmL.length != pcmR.length) {
            throw new IllegalArgumentException();
        }
        this.imageIndex = imageIndex;
        this.pcmL = pcmL;
        this.pcmR = pcmR;
    }

    public int getImageIndex() {
        return imageIndex;
    }

    public int getFrameCount() {
        return pcmL.length;
    }

    public short[] getPcmL() {
        return pcmL;
    }

    public short[] getPcmR() {
        return pcmR;
    }

    public short getLastL() {
        // no bound check
        return pcmL[pcmL.length - 1];
    }

    public short getLastR() {
        // no bound check
        return pcmR[pcmR.length - 1];
    }
}
