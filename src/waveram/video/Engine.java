package waveram.video;

import waveram.video.algos.Algorithm;
import waveram.video.data.*;
import waveram.video.util.TimeSampler;

import javax.imageio.ImageIO;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Engine {

    private static final int WRITE_CONCURRENCY = 3;

    public void render(Song song, Algorithm algorithm, Path outputRootFolder, VideoFormat videoFormat)
            throws IOException, UnsupportedAudioFileException, InterruptedException {

        final Path videoFolder = outputRootFolder.resolve(song.getShortName() + "_"
                + algorithm.getShortName() + "_" + videoFormat.getFolderName());
        Files.createDirectories(videoFolder);
        System.out.println("To: " + videoFolder);

        final TimeSampler timeSampler = new TimeSampler();

        final Pcm pcm = new PcmLoader().loadPcm(song.getInputFile(), videoFormat.fps, algorithm.carryOverLastSample());
        System.out.println("\tTime to load PCM: " + timeSampler.pollMicros() + " micros");

        final java.util.List<PcmForImage> pcmList = pcm.getPcmList();
        final List<SpaceForImage> spaceList = new ArrayList<>();
        for (PcmForImage pcmForImage : pcmList) {
            spaceList.add(SpaceForImage.create(pcmForImage, ScopeMode.Vertical));
        }
        System.out.println("\tTime to convert PCM: " + timeSampler.pollMicros() + " micros");

        final Renderer renderer = algorithm.prepare(spaceList, videoFormat);
        System.out.println("\tTime to preprocess: " + timeSampler.pollMicros() + " micros");

        final int encoderThreadCount = Runtime.getRuntime().availableProcessors();
        final ExecutorService workers = Executors.newFixedThreadPool(encoderThreadCount);
        System.out.println("\tNumber of threads: " + encoderThreadCount);

        final Semaphore writeSemaphore = new Semaphore(WRITE_CONCURRENCY);

        // Later, maybe decouple file index from image index (allow writing some stuff before stream starts)
        final int endImageIndex = spaceList.size();
        final int outputIndexOffset = 0;
        AtomicInteger nextImageIndex = new AtomicInteger(0);

        for (int i = 0; i < encoderThreadCount; i++) {
            workers.execute(() -> {

                final TimeSampler timeSampler1 = new TimeSampler();

                final ByteArrayOutputStream baos = new ByteArrayOutputStream(64 * 1024);

                final ImageBuffer imageBuffer = new ImageBuffer(videoFormat.w, videoFormat.h);
                renderer.initBuffer(imageBuffer);
                final long microsToInit = timeSampler1.pollMicros();

                int frameCount = 0;
                long microsToReset = 0;
                long microsToRender = 0;
                long microsToEncode = 0;
                long microsToWait = 0;
                long microsToWrite = 0;

                while (true) {
                    final int imageIndex = nextImageIndex.getAndIncrement();
                    if (imageIndex >= endImageIndex) {
                        break;
                    }
                    if (imageIndex > 0 && (imageIndex % 250) == 0) {
                        System.out.print(imageIndex + " (" + ((100 * imageIndex) / endImageIndex) + "%)...\r");
                        System.out.flush();
                    }

                    frameCount++;

                    renderer.resetBuffer(imageBuffer);
                    microsToReset += timeSampler1.pollMicros();

                    renderer.render(imageBuffer, imageIndex);
                    microsToRender += timeSampler1.pollMicros();

                    try {
                        baos.reset();
                        ImageIO.write(imageBuffer.image, "png", baos);
                        microsToEncode += timeSampler1.pollMicros();

                        final String indexString = getIndexString(imageIndex + outputIndexOffset);
                        final String imageName = algorithm.getShortName() + "_(" + indexString + ").png";
                        final Path path = videoFolder.resolve(imageName);

                        // Write to disk, with concurrency limited by Semaphore (todo: no memory implications?!)
                        writeSemaphore.acquireUninterruptibly();
                        microsToWait += timeSampler1.pollMicros();
                        try {
                            try (final OutputStream fos = Files.newOutputStream(path)) {
                                baos.writeTo(fos);
                            }
                            microsToWrite += timeSampler1.pollMicros();
                        } finally {
                            writeSemaphore.release();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.exit(1);
                    }
                }

                System.out.println("\tframes=" + frameCount
                        + ", microsToInit=" + (microsToInit / frameCount)
                        + ", microsToReset=" + (microsToReset / frameCount)
                        + ", microsToRender=" + (microsToRender / frameCount)
                        + ", microsToEncode=" + (microsToEncode / frameCount)
                        + ", microsToWait=" + (microsToWait / frameCount)
                        + ", microsToWrite=" + (microsToWrite / frameCount));
            });
        }

        System.out.println("\tWaiting for threads...");
        workers.shutdown();
        //noinspection ResultOfMethodCallIgnored
        workers.awaitTermination(1, TimeUnit.HOURS);

        System.out.println("\tDone");
    }

    private static String getIndexString(int imageIndex) {
        String nr = String.valueOf(imageIndex);
        while (nr.length() < 5) {
            //noinspection StringConcatenationInLoop
            nr = "0" + nr;
        }
        return nr;
    }

}
