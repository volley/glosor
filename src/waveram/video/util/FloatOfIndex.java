package waveram.video.util;

public final class FloatOfIndex implements Comparable<FloatOfIndex> {
    public int index;
    public float sortOn;

    public FloatOfIndex(int index, float sortOn) {
        this.index = index;
        this.sortOn = sortOn;
    }

    @Override
    public int compareTo(FloatOfIndex o) {
        return Float.compare(sortOn, o.sortOn);
    }
}
