package waveram.video.util;

public class TimeSampler {
    private long last = System.nanoTime();

    public long pollMicros() {
        long now = System.nanoTime();
        long deltaMs = (now - last) / 1000L;
        last = now;
        return deltaMs;
    }
}
