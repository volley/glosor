package waveram.video.util;

import java.awt.*;

public final class FontUtil {

    static GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    static Font[] fonts = ge.getAllFonts();

    static {
        //printAllFonts();
        System.out.println("Found " + fonts.length + " fonts");
    }

    private static void printAllFonts() {
        System.out.println("Fonts:");
        for (Font font : fonts) {
            System.out.println("\t" + font);
        }
    }

    public static Font selectFont(String match, float size) {
        for (Font font : fonts) {
            if (font.toString().contains(match)) {
                System.out.println("Selecting font for \"" + match + "\": " + font);
                return font.deriveFont(size);
            }
        }

        throw new IllegalArgumentException("No such font: " + match);
    }
}
