package waveram.video.util;

public final class IntOfIndex implements Comparable<IntOfIndex> {
    public int index;
    public int sortOn;

    public IntOfIndex(int index, int sortOn) {
        this.index = index;
        this.sortOn = sortOn;
    }

    @Override
    public int compareTo(IntOfIndex o) {
        return Integer.compare(sortOn, o.sortOn);
    }
}
