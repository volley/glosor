package waveram.video.algos;

import waveram.video.Renderer;
import waveram.video.VideoFormat;
import waveram.video.data.SpaceForImage;

import java.util.List;

public interface Algorithm {

    String getShortName();

    boolean carryOverLastSample();

    Renderer prepare(List<SpaceForImage> list, VideoFormat videoFormat);

}
