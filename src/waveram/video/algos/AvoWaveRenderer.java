package waveram.video.algos;

import waveram.MathUtil;
import waveram.video.ImageBuffer;
import waveram.video.Renderer;
import waveram.video.VideoFormat;
import waveram.video.data.Compressor;
import waveram.video.data.SpaceForImage;
import waveram.video.util.FloatOfIndex;
import waveram.video.util.FontUtil;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class AvoWaveRenderer implements Renderer {

    static final Color backgroundColor = new Color(0, 0, 0);

    private final VideoFormat videoFormat;
    private final int bpm;
    private final float zoomFactor;
    private final int fontSize;
    private final Font font;

    private BasicStroke mainStroke;
    private BasicStroke fadeStroke1;
    private BasicStroke fadeStroke2;

    private static final int fadeStroke1Factor = 3;
    private static final int fadeStroke2Factor = 6;

    private final List<SpaceForImage> originalList;
    private final List<SpaceForImage> screenList;
    private final List<float[]> anglesInOriginal;
    private final List<float[]> distancesInOriginal;

    private static final boolean allowZoom = true;

    private static int getFontSize(VideoFormat videoFormat) {
        if (videoFormat.w <= 2000) {
            return 24;
        } else {
            return 48;
        }
    }

    private static int getStrokeWidth(VideoFormat videoFormat) {
        if (videoFormat.w <= 2000) {
            return 1;
        } else {
            return 2;
        }
    }

    static float getZoomInFactorAt(int index) {
        if (index > 8700) {
            return 1.00045f;
        }
        if (index > 8650) {
            return 1.0009f;
        }
        if (index > 8600) {
            return 1.0018f;
        }
        if (index > 8550) {
            return 1.0035f;
        }
        if (index > 8500) {
            return 1.007f;
        }
        return index < 8200 ? 1.007f : 1.012f;
    }

    static float getZoomOutFactorAt(int index) {
        return index < 8200 ? 1.007f : 1.005f;
    }

    AvoWaveRenderer(List<SpaceForImage> list, VideoFormat videoFormat, int bpm) {
        this.originalList = list;
        this.videoFormat = videoFormat;
        this.bpm = bpm;
        this.fontSize = getFontSize(videoFormat);
        this.font = FontUtil.selectFont("family=Courier New", fontSize);

        // Make a copy of the space scape coordinates
        screenList = new ArrayList<>();
        for (SpaceForImage spaceForImage : originalList) {
            screenList.add(spaceForImage.copy());
        }

        // Apply dynamics to the space scope
        final float screenMax = videoFormat.h / 2.0f;
        zoomFactor = 65535.0f / screenMax;
        if (allowZoom) {
            Compressor.compress(screenList, screenMax, AvoWaveRenderer::getZoomInFactorAt, AvoWaveRenderer::getZoomOutFactorAt);
            //Compressor.compress(screenList, screenMax, 1.008f, 1.008f);
        } else {
            for (SpaceForImage space : screenList) {
                space.scale(0.01f);
            }
        }
        for (SpaceForImage spaceForImage : screenList) {
            spaceForImage.scaleX((1.1f * videoFormat.w) / videoFormat.h);
            spaceForImage.translate(videoFormat.w / 2.0f, videoFormat.h / 2.0f);
        }

        anglesInOriginal = new ArrayList<>();
        distancesInOriginal = new ArrayList<>();
        for (SpaceForImage space : originalList) {
            final int audioFramesInImage = space.getFrameCount();
            float[] as = new float[audioFramesInImage];
            float[] ds = new float[audioFramesInImage];
            as[0] = Float.NaN;
            ds[0] = Float.NaN;
            final float[] xs = space.getXs();
            final float[] ys = space.getYs();
            for (int i = 1; i < audioFramesInImage; i++) {
                final float dx = xs[i] - xs[i - 1];
                final float dy = ys[i] - ys[i - 1];
                as[i] = (float) Math.atan2(Math.abs(dy), Math.abs(dx)); // radians, 0..pi/2
                ds[i] = Math.min(90, (float) Math.sqrt(MathUtil.distance(dx, dy))) / 90.0f;
            }
            anglesInOriginal.add(as);
            distancesInOriginal.add(ds);
        }
    }

    @Override
    public void initBuffer(ImageBuffer imageBuffer) {
        imageBuffer.graphics.setFont(font);
        mainStroke = new BasicStroke(getStrokeWidth(videoFormat));
        fadeStroke1 = new BasicStroke(getStrokeWidth(videoFormat) * fadeStroke1Factor);
        fadeStroke2 = new BasicStroke(getStrokeWidth(videoFormat) * fadeStroke2Factor);
    }

    @Override
    public void resetBuffer(ImageBuffer imageBuffer) {
        imageBuffer.graphics.setBackground(backgroundColor); // no alpha
        imageBuffer.graphics.clearRect(0, 0, videoFormat.w, videoFormat.h);
    }

    private static String padLeft(int n) {
        String str = Integer.toString(n);
        while (str.length() < 4) {
            //noinspection StringConcatenationInLoop
            str = "0" + str;
        }
        return str;
    }

    private static String getZoomCookie(float zoom) {
        String f = Float.toString(zoom);
        int dotAt = f.indexOf('.');
        if (dotAt > 0) {
            f = f.substring(0, dotAt) + f.substring(dotAt + 1);
        }
        while (f.length() < 4) {
            //noinspection StringConcatenationInLoop
            f = f + "0";
        }
        f = f.substring(0, 4);
        return f;
    }

    private void renderOld(ImageBuffer imageBuffer, int imageIndex, float strength) {
        final Graphics2D graphics = imageBuffer.graphics;

        final SpaceForImage screenXys = screenList.get(imageIndex);
        final int audioFramesInImage = screenXys.getFrameCount();

        for (int i = 1; i < audioFramesInImage; i++) {

            //final float angle = anglesInOriginal.get(imageIndex)[i];
            final float distance = distancesInOriginal.get(imageIndex)[i];

            float blue = MathUtil.clampUpper(distance * 1.2f);
            if (blue > 0.005f) {
                Color c = new Color(0f, 0f, 1f, blue * strength);
                graphics.setPaint(c);

                int x0 = Math.round(screenXys.getXs()[i - 1]);
                int y0 = Math.round(screenXys.getYs()[i - 1]);
                int x1 = Math.round(screenXys.getXs()[i]);
                int y1 = Math.round(screenXys.getYs()[i]);
                graphics.drawLine(x0, y0, x1, y1);
            }
        }
    }

    @Override
    public void render(ImageBuffer imageBuffer, int imageIndex) {
        final Graphics2D graphics = imageBuffer.graphics;

        if (imageIndex > 0) {
            if (imageIndex > 1) {
                if (imageIndex > 2) {
                    if (imageIndex > 3) {
                        graphics.setStroke(fadeStroke2);
                        renderOld(imageBuffer, imageIndex - 4, 0.075f / fadeStroke2Factor);
                    }
                    graphics.setStroke(fadeStroke1);
                    renderOld(imageBuffer, imageIndex - 3, 0.15f / fadeStroke1Factor);
                }
                graphics.setStroke(mainStroke);
                renderOld(imageBuffer, imageIndex - 2, 0.3f);
            }
            graphics.setStroke(mainStroke);
            renderOld(imageBuffer, imageIndex - 1, 0.6f);
        }

        graphics.setStroke(mainStroke);

        final SpaceForImage originalXys = originalList.get(imageIndex);
        final SpaceForImage screenXys = screenList.get(imageIndex);

        final int audioFramesInImage = originalXys.getFrameCount();
        final ArrayList<FloatOfIndex> list = new ArrayList<>(audioFramesInImage - 1);
        for (int i = 1; i < audioFramesInImage; i++) {
            list.add(new FloatOfIndex(i, -distancesInOriginal.get(imageIndex)[i]));
        }
        Collections.sort(list);

        for (FloatOfIndex floatOfIndex : list) {
            final int i = floatOfIndex.index;

            final float angle = anglesInOriginal.get(imageIndex)[i];
            final float distance = distancesInOriginal.get(imageIndex)[i];

            Color c = new Color(MathUtil.clampUpper(1.05f - distance), MathUtil.clampUpper(1.05f - (angle / 1.53f)), MathUtil.clampUpper(distance * 1.2f));
            graphics.setPaint(c);

            int x0 = Math.round(screenXys.getXs()[i - 1]);
            int y0 = Math.round(screenXys.getYs()[i - 1]);
            int x1 = Math.round(screenXys.getXs()[i]);
            int y1 = Math.round(screenXys.getYs()[i]);

            for (int deltax = 0; deltax < 2; deltax++) {
                for (int deltay = 0; deltay < 2; deltay++) {
                    graphics.drawLine(x0 + deltax, y0 + deltay, x1 + deltax, y1 + deltay);
                }
            }
        }

        graphics.setPaint(new Color(255, 255, 255, 13));
        for (int i = 0; i < audioFramesInImage; i++) {
            int x0 = Math.round(screenXys.getXs()[i]);
            int y0 = Math.round(screenXys.getYs()[i]);
            graphics.drawLine(x0, y0, x0 + 1, y0);
            graphics.drawLine(x0, y0 + 1, x0 + 1, y0 + 1);
        }

        final int width = graphics.getFontMetrics().stringWidth("0000");

        final int tick = (imageIndex * bpm) / (60 * videoFormat.fps);
        final int tick4 = tick % 4;
        final String tickString = "|/-\\".substring(tick4, tick4 + 1);
        //graphics.drawString(tickString, 64, videoFormat.h - 48 - fontSize);

        graphics.setPaint(Color.WHITE);
        graphics.drawString(padLeft(imageIndex) + " " + tickString, 64, videoFormat.h - 48 - fontSize);
        graphics.drawString(getZoomCookie(screenXys.getZoom() * zoomFactor),
                videoFormat.w - 64 - width, videoFormat.h - 48 - fontSize);

    }

}
