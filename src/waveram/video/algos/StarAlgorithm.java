package waveram.video.algos;

import waveram.video.Renderer;
import waveram.video.VideoFormat;
import waveram.video.data.SpaceForImage;

import java.util.List;

public class StarAlgorithm implements Algorithm {

    public StarAlgorithm() {
    }

    @Override
    public String getShortName() {
        return "Star";
    }

    @Override
    public boolean carryOverLastSample() {
        return false;
    }

    @Override
    public Renderer prepare(List<SpaceForImage> list, VideoFormat videoFormat) {
        return new StarRenderer(list, videoFormat);
    }

}
