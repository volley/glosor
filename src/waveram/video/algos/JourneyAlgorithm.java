package waveram.video.algos;

import waveram.video.Renderer;
import waveram.video.VideoFormat;
import waveram.video.data.SpaceForImage;

import java.util.List;

public class JourneyAlgorithm implements Algorithm {

    private final int bpm;

    public JourneyAlgorithm(int bpm) {
        this.bpm = bpm;
    }

    @Override
    public String getShortName() {
        return "Journey";
    }

    @Override
    public boolean carryOverLastSample() {
        return false;
    }

    @Override
    public Renderer prepare(List<SpaceForImage> list, VideoFormat videoFormat) {
        return new JourneyRenderer(list, videoFormat, bpm);
    }

}
