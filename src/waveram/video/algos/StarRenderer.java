package waveram.video.algos;

import waveram.MathUtil;
import waveram.video.ImageBuffer;
import waveram.video.Renderer;
import waveram.video.VideoFormat;
import waveram.video.data.Compressor;
import waveram.video.data.SpaceForImage;
import waveram.video.util.FloatOfIndex;
import waveram.video.util.FontUtil;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class StarRenderer implements Renderer {

    static final Color backgroundColor = new Color(0, 0, 0);

    private final List<SpaceForImage> list;
    private final VideoFormat videoFormat;
    private final Font font;

    StarRenderer(List<SpaceForImage> list, VideoFormat videoFormat) {
        this.videoFormat = videoFormat;
        this.font = FontUtil.selectFont("family=Courier New", 24);

        this.list = new ArrayList<>();
        for (SpaceForImage input : list) {
            final int frameCount = input.getFrameCount();
            float[] xs = new float[frameCount - 1];
            float[] ys = new float[frameCount - 1];
            for (int i = 1; i < frameCount; i++) {
                xs[i - 1] = input.getXs()[i] - input.getXs()[i - 1];
                ys[i - 1] = input.getYs()[i] - input.getYs()[i - 1];
            }
            this.list.add(new SpaceForImage(input.getImageIndex(), xs, ys));
        }

        Compressor.compress(this.list, 65535.0f, 1.008f, 1.008f);
    }

    @Override
    public void initBuffer(ImageBuffer imageBuffer) {
        imageBuffer.graphics.setFont(font);
    }

    @Override
    public void resetBuffer(ImageBuffer imageBuffer) {
        imageBuffer.graphics.setBackground(backgroundColor); // no alpha
        imageBuffer.graphics.clearRect(0, 0, videoFormat.w, videoFormat.h);
    }

    @Override
    public void render(ImageBuffer imageBuffer, int imageIndex) {
        final Graphics2D graphics = imageBuffer.graphics;
        final SpaceForImage spaceForImage = list.get(imageIndex);

        final int audioFramesInImage = spaceForImage.getFrameCount();

        final float widthScale = ((videoFormat.w / 65536.0f) / 2.0f);
        final float heightScale = ((videoFormat.h / 65536.0f) / 2.0f);

        final float[] spaceXs = spaceForImage.getXs();
        final float[] spaceYs = spaceForImage.getYs();

        final int midX = videoFormat.w / 2;
        final int midY = videoFormat.h / 2;

        final int[] screenXs = new int[audioFramesInImage];
        final int[] screenYs = new int[audioFramesInImage];
        for (int i = 0; i < audioFramesInImage; i++) {
            screenXs[i] = midX + Math.round(spaceXs[i] * widthScale);
            screenYs[i] = midY + Math.round(spaceYs[i] * heightScale);
        }

        final List<FloatOfIndex> list = new ArrayList<>(audioFramesInImage);
        for (int i = 0; i < audioFramesInImage; i++) {
            list.add(new FloatOfIndex(i, -MathUtil.distanceSquared(spaceXs[i], spaceYs[i])));
        }
        Collections.sort(list);

        for (FloatOfIndex floatOfIndex : list) {
            final int i = floatOfIndex.index;
            final float dx = spaceXs[i];
            final float dy = spaceYs[i];

            final float angle = (float) Math.atan2(Math.abs(dy), Math.abs(dx)); // radians, 0..pi/2
            final float distance = Math.min(90, (float) Math.sqrt(MathUtil.distance(dx, dy))) / 90.0f;

            Color c = new Color(MathUtil.clampUpper(1.05f - distance), MathUtil.clampUpper(1.05f - (angle / 1.53f)), MathUtil.clampUpper(distance * 1.2f));
            graphics.setPaint(c);
            for (int deltax = 0; deltax < 2; deltax++) {
                for (int deltay = 0; deltay < 2; deltay++) {
                    graphics.drawLine(midX + deltax, midY + deltay, screenXs[i] + deltax, screenYs[i] + deltay);
                }
            }
        }

        //graphics.setPaint(new Color(255, 255, 255, 13));
        //for (int i = 0; i < audioFramesInImage; i++) {
//            graphics.drawLine(screenXs[i], screenYs[i], screenXs[i] + 1, screenYs[i]);
//            graphics.drawLine(screenXs[i], screenYs[i] + 1, screenXs[i] + 1, screenYs[i] + 1);
//        }
    }

}
