package waveram.video.algos;

import waveram.video.Renderer;
import waveram.video.VideoFormat;
import waveram.video.data.SpaceForImage;

import java.util.List;

public class AvoWaveAlgorithm implements Algorithm {

    private final int bpm;

    public AvoWaveAlgorithm(int bpm) {
        this.bpm = bpm;
    }

    @Override
    public String getShortName() {
        return "AvoWave";
    }

    @Override
    public boolean carryOverLastSample() {
        return true;
    }

    @Override
    public Renderer prepare(List<SpaceForImage> list, VideoFormat videoFormat) {
        return new AvoWaveRenderer(list, videoFormat, bpm);
    }

}
