package waveram.video.algos;

import waveram.video.ImageBuffer;
import waveram.video.Renderer;
import waveram.video.VideoFormat;
import waveram.video.data.Compressor;
import waveram.video.data.SpaceForImage;
import waveram.video.util.FontUtil;

import java.awt.*;
import java.util.List;

class JourneyRenderer implements Renderer {

    static final Color backgroundColor = new Color(0, 0, 0);

    private final List<SpaceForImage> list;
    private final VideoFormat videoFormat;
    private final int bpm;
    private final Font font;

    private final float[] ax;
    private final float[] ay;
    private final float[] az;

    JourneyRenderer(List<SpaceForImage> list, VideoFormat videoFormat, int bpm) {
        this.list = list;
        this.videoFormat = videoFormat;
        this.bpm = bpm;
        this.font = FontUtil.selectFont("family=Courier New", videoFormat.w > 1920 ? 32 : 24);

        final int sampleFramesCount = 44100 * 5;
        ax = new float[sampleFramesCount]; // x center for sample frame n
        ay = new float[sampleFramesCount]; // y center for sample frame n
        az = new float[sampleFramesCount]; // zoom for sample frame n

        double tx = (-videoFormat.w * 2) / 3f; // travel
        double ty = videoFormat.h / 3.0f;
        double z = 1.0;
        final double xo = videoFormat.w / 3f - tx;
        final double yo = videoFormat.h / 1.7f - ty;
        double fade = 0.999992;
        for (int i = 0; i < sampleFramesCount; i++) {
            ax[i] = (float) (xo + tx);
            ay[i] = (float) (yo + ty);
            az[i] = (float) z;
            tx = tx * fade;
            ty = ty * fade;
            z = z * fade;
        }

        Compressor.compress(list, Math.min(videoFormat.w, videoFormat.h) / 2f,
                1.003f, 1.003f);
    }

    @Override
    public void initBuffer(ImageBuffer imageBuffer) {
        imageBuffer.graphics.setFont(font);
    }

    @Override
    public void resetBuffer(ImageBuffer imageBuffer) {
        imageBuffer.graphics.setBackground(backgroundColor); // no alpha
        imageBuffer.graphics.clearRect(0, 0, videoFormat.w, videoFormat.h);
    }

    private static final Color currentColor = new Color(255, 255, 255, 128);

    @Override
    public void render(ImageBuffer imageBuffer, int imageIndex) {
        final Graphics2D graphics = imageBuffer.graphics;

        graphics.setPaint(Color.WHITE);
        graphics.drawString(Integer.toString(imageIndex), 64, 64);

        int relativeIndex = 0;
        for (; imageIndex < list.size(); imageIndex++) {
/*
            graphics.setPaint(new Color(0, 255, 0, 100));
            {
                int sx = Math.round(ax[relativeIndex]);
                int sy = Math.round(ay[relativeIndex]);
                graphics.drawLine(sx - 1, sy, sx + 1, sy);
                graphics.drawLine(sx, sy - 1, sx, sy + 1);
            }
 */

            float fraction = ((float) (ax.length - relativeIndex)) / ax.length;
            fraction = fraction * fraction * fraction * fraction;

            if (relativeIndex == 0) {
                graphics.setPaint(currentColor);
            } else {
                graphics.setPaint(new Color(1.0f, 0.9f, 0, 0.2f * fraction));
            }

            final SpaceForImage spaceForImage = list.get(imageIndex);
            final int audioFramesInImage = spaceForImage.getFrameCount();
            final float[] xs = spaceForImage.getXs();
            final float[] ys = spaceForImage.getYs();
            for (int i = 0; i < audioFramesInImage; i++) {
                final int sx = Math.round((az[relativeIndex] * xs[i]) + ax[relativeIndex]);
                final int sy = Math.round((az[relativeIndex] * ys[i]) + ay[relativeIndex]);
                graphics.drawLine(sx - 1, sy, sx + 1, sy);
                graphics.drawLine(sx, sy - 1, sx, sy + 1);
                relativeIndex++;
                if (relativeIndex == ax.length) {
                    return;
                }
            }
        }
    }

}
