package vocab;

class ActiveWord {

    final Word word;

    int correctStreak; // antal rätt i följd (utan fel mellan)
    int wrongStreak;   // antal fel i följd (utan rätt mellan)

    ActiveWord(Word word) {
        this.word = word;
    }
}
