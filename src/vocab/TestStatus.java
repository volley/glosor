package vocab;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestStatus {

    private final Random random = new Random();

    // Ord som vi ännu inte börjat förhöra, men kommer att förhöra
    private final List<Word> remainingWords = new ArrayList<>();

    // Ord som vi just nu håller på att förhöra
    private final List<ActiveWord> activeWords = new ArrayList<>();

    private int incorrectCount = 0;

    TestStatus(List<Word> words) {
        remainingWords.addAll(words);
    }

    boolean isDone() {
        // De dubbla &-tecknen läses "och"
        return remainingWords.isEmpty() && activeWords.isEmpty();
    }

    ActiveWord selectWord() {

        activateWordsIfNeeded();

        // För att undvika att fråga om samma ord flera gånger i rad, så
        // väljer vi ord från listans början och flyttar hela tiden det
        // ordet vi frågar om till slutet.

        // Bestäm hur många av de första orden vi väljer bland.
        int optionCount = getOptionCount();

        // Slumpa ett tal (talet kan vara från och med 0 och upp till optionCount,
        // men inte inklusive; det är alltså alltid mindre än optionCount).
        int randomIndex = random.nextInt(optionCount);

        // Ta bort ordet ur listan.
        ActiveWord selectedWord = activeWords.remove(randomIndex);

        // Lägg till ordet igen, allra sist
        activeWords.add(selectedWord);

        return selectedWord;
    }

    private int getOptionCount() {
        int activeWordCount = activeWords.size();
        if (activeWordCount == 1) {
            return 1;
        } else if (activeWordCount <= 3) {
            return activeWordCount - 1;
        } else {
            return activeWordCount / 2;
        }
    }

    private void activateWordsIfNeeded() {

        // Här bestäms hur många ord vi som mest frågar om samtidigt.
        // Om man har svarat fel flera gånger så är det kanske bättre
        // att jobba med färre ord i taget.
        int maxActiveCount;
        if (incorrectCount < 5) {
            maxActiveCount = 6;
        } else {
            maxActiveCount = 5;
        }

        // < betyder "mindre än" ("less than" på Engelska)
        // && läses "och" ("and" på Engelska)
        // ! läses "inte" ("not" på Engelska)
        while (activeWords.size() < maxActiveCount && !remainingWords.isEmpty()) {
            activateOneRandomWord();
        }
    }

    private void activateOneRandomWord() {
        int position = random.nextInt(remainingWords.size());
        Word word = remainingWords.remove(position);
        activeWords.add(new ActiveWord(word));
    }

    boolean onCorrect(ActiveWord activeWord) {
        activeWord.correctStreak++;
        activeWord.wrongStreak = 0;

        boolean isDone = activeWord.correctStreak == 3;

        if (isDone) {
            onActiveWordDone(activeWord);
        }

        return isDone;
    }

    void onIncorrect(ActiveWord activeWord) {
        activeWord.correctStreak = 0;
        activeWord.wrongStreak++;

        incorrectCount++;
    }

    private void onActiveWordDone(ActiveWord activeWord) {
        activeWords.remove(activeWord);

        if (activeWord.word.andThen != null) {
            remainingWords.add(activeWord.word.andThen);
        }
    }

}
