package vocab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.text.Collator;

class TestRunner {

    private final BufferedReader in;
    private final PrintStream out;

    private final TestStatus testStatus;

    TestRunner(BufferedReader in, PrintStream out, TestStatus testStatus) {
        this.in = in;
        this.out = out;
        this.testStatus = testStatus;
    }

    void runTest() throws IOException {
        while (!testStatus.isDone()) {

            // Välj nästa ord att testa
            ActiveWord activeWord = testStatus.selectWord();
            Word word = activeWord.word;

            // Skriv ut frågan
            String message = word.from;
            if (word.hasContext()) {
                message += " (" + word.context + ")";
            }
            out.println();
            out.println(message);

            // Skriv även ut en hint, om man nyss svarat fel
            if (activeWord.wrongStreak > 0) {
                if (word.hasHint()) {
                    out.println("Tips: " + word.hint);
                } else if (word.to.length() > 3) {
                    out.println("Tips: " + word.to.substring(0, 2) + "...");
                }
            }

            // Skriv ut en prompt
            out.print(": ");
            out.flush();

            // Läs användarens svar
            String answer = in.readLine();

            // Rätta
            if (answer.length() > 0) {
                if (answer.equals(word.to)) {
                    // Rätt svar givet
                    boolean isDone = testStatus.onCorrect(activeWord);
                    if (isDone) {
                        out.println("Yes!");
                    } else if (activeWord.correctStreak == 1) {
                        out.println("R\u00e4tt!");
                    } else {
                        out.println("R\u00e4tt igen!");
                    }
                } else if ((answer + ".").equals(word.to)) {
                    out.println("Saknas bara en punkt: " + word.to);
                } else if ((answer + "..").equals(word.to)) {
                    out.println("Saknas bara punkter: " + word.to);
                } else if ((answer + "...").equals(word.to)) {
                    out.println("Saknas bara punkter: " + word.to);
                } else if ((answer + "!").equals(word.to)) {
                    out.println("Saknas bara ett utropstecken: " + word.to);
                } else if ((answer + "?").equals(word.to)) {
                    out.println("Saknas bara ett fr\u00e5getecken: " + word.to);
                } else if (("the " + answer).equals(word.to)) {
                    out.println("Saknas bara best\u00e4md form: " + word.to);
                } else if (justAccents(answer, word.to)) {
                    out.println("Skillnad i accent: " + word.to);
                } else if (answer.length() > 1 &&
                        (answer.substring(0, 1).toUpperCase() +
                                answer.substring(1)).equals(word.to)) {
                    out.println("Saknas bara stor begynnelsebokstav: " + word.to);
                } else {
                    // Fel svar givet
                    out.println("R\u00e4tt svar \u00e4r: " + word.to);
                    testStatus.onIncorrect(activeWord);
                }
            } else {
                // Inget svar gavs. Gör inget, bara skriv ut rätt svar.
                out.println("R\u00e4tt svar \u00e4r: " + word.to);
            }
        }
    }

    private static boolean justAccents(String a, String b) {
        final Collator instance = Collator.getInstance();
        instance.setStrength(Collator.NO_DECOMPOSITION);
        return instance.compare(a, b) == 0;
    }

}
