package vocab;

import java.awt.*;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static void load(Path path, List<Word> words) throws IOException {
        Word lastWord = null;

        for (String line : Files.readAllLines(path)) {

            // Skip comments and empty lines
            line = line.trim();
            if (line.startsWith("#") || line.isEmpty()) {
                continue;
            }

            boolean isContinuation = line.startsWith("...") && lastWord != null;
            if (isContinuation) {
                line = line.substring(3);
            }

            String context = null;
            int bracketOpenPos = line.indexOf('[');
            if (bracketOpenPos >= 0) {
                int bracketClosePos = line.indexOf(']', bracketOpenPos);
                if (bracketClosePos >= 0) {
                    context = line.substring(bracketOpenPos + 1, bracketClosePos).trim();
                    line = line.substring(0, bracketOpenPos) +
                            line.substring(bracketClosePos + 1);
                    line = line.trim();
                }
            }

            int equalsPos = line.indexOf("=");
            if (equalsPos < 0) {
                throw new IOException(path + ": Line without =: " + line);
            }
            String from = line.substring(0, equalsPos).trim();
            String to = line.substring(equalsPos + 1).trim();

            Word word = new Word(from, to);
            if (context != null) {
                word.context(context);
            }

            if (isContinuation) {
                lastWord.andThen(word);
            } else {
                words.add(word);
            }

            lastWord = word;
        }
    }

    private static File[] selectFile() {
        Frame parent = null;
        FileDialog fileDialog = new FileDialog(parent, "V\u00e4lj glosfil(er)");
        fileDialog.setFilenameFilter((dir, name) -> name.toLowerCase().endsWith(".txt"));
        fileDialog.setMultipleMode(true);
        fileDialog.setVisible(true);
        return fileDialog.getFiles();
    }

    public static void main(String[] args) throws IOException {
        List<Word> words = new ArrayList<>();

        if (args.length > 0) {
            // Vi fick argument till oss, låt oss anta att det är filnamn
            for (String arg : args) {
                load(FileSystems.getDefault().getPath(arg), words);
            }
        } else {
            // Vi fick inga argument, så fråga efter en fil (eller flera filer)
            File[] files = selectFile();
            if (files != null) {
                for (File file : files) {
                    load(FileSystems.getDefault().getPath(file.getAbsolutePath()), words);
                }
            }
        }

        if (words.isEmpty()) {
            System.out.println("Inga glosor...");
            System.exit(0); // kill awt
        }

        TestStatus testStatus = new TestStatus(words);

        try (Reader reader = new InputStreamReader(System.in)) {
            BufferedReader in = new BufferedReader(reader);
            TestRunner testRunner = new TestRunner(in, System.out, testStatus);
            testRunner.runTest();
        }

        System.out.println();
        System.out.println("Boom!");
    }

}
