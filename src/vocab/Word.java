package vocab;

// Beskriver en glosa att lära sig.
// Det kan vara ett ord, flera ord eller en hel mening.
class Word {

    final String from; // "från"-ordet, tex "barn"
    final String to;   // "till"-ordet, tex "children"

    String context; // ett sammanhang, tex "plural, dvs mer än ett barn"
    String hint; // ett tips

    Word andThen;

    Word(String from, String to) {
        this.from = from;
        this.to = to;
    }

    Word context(String context) {
        this.context = context;
        return this;
    }

    boolean hasContext() {
        return context != null;
    }

    Word hint(String hint) {
        this.hint = hint;
        return this;
    }

    boolean hasHint() {
        return hint != null;
    }

    Word andThen(Word word) {
        assert andThen == null : "For now we can only chain one word";
        andThen = word;
        return this;
    }

    static Word of(String from, String to) {
        return new Word(from, to);
    }

}
